// URL ajout https://discordapp.com/oauth2/authorize?client_id=695582556149776424&scope=bot&permissions=523328
function getDigitEmoji(n) {
    n = n.toString();
    var s = '';
    for( let i = 0; i < n.length; i++){
        switch (n[i]) {
            case '1':
                s = s+'1️⃣';
				break;
            case '2':
                s = s+'2️⃣';
				break;
            case '3':
                s = s+'3️⃣';
				break;
            case '4':
                s = s+'4️⃣';
				break;
            case '5':
                s = s+'5️⃣';
				break;
            case '6':
                s = s+'6️⃣';   
				break;
            case '7':
                s = s+'7️⃣';
				break;
            case '8':
                s = s+'8️⃣';
				break;
            case '9':
                s = s+'9️⃣';
				break;
            case '0':
                s = s+'0️⃣';
				break;
        }
    }
    return s;
}
class Player {
    constructor(Game, id, name) {
        this.game = Game;
        this.id = id;
        this.name = '<@'+id+'>';
        this.duel = 0;
        this.roles = {dieu: null, heros: null, oracle: null, ecuyer: null, prisonnier: null, catin: null, aubergiste: null, princesse: null, dragon: null, clochard: null, devin: null, apprenti: null, gourgandine: null, imperatrice: null, demon: null};
        this.rolesHistory = {dieu: 0, heros: 0, oracle: 0, ecuyer: 0, prisonnier: 0, catin: 0, aubergiste: 0, princesse: 0, dragon: 0, clochard: 0, devin: 0, apprenti: 0, gourgandine: 0, imperatrice: 0, demon: 0};
    }

    getRoles() {
        let roles = '';
        for (let role in this.roles) {
            if (this.roles[role] !== null) {
                roles = roles+', '+role;
            }
        }
        roles = roles.slice(2);
        if (roles === '') {
            roles = 'n’a aucun role';
        } else {
            roles = 'a comme role : '+roles;
        }
        return roles;
    }
    getRolesHistory() {
        let roles = '';
        let Player = this;
        for (let role in this.rolesHistory) {
            if (Player.rolesHistory[role] != 0) {
                if (Player.rolesHistory[role] == 1) {
                    roles = roles+', '+role+' une fois';
                } else {
                    roles = roles+', '+role+' '+Player.rolesHistory[role]+' fois';
                }
            }
        }
        roles = roles.slice(2);
        if (roles === '') {
            roles = 'n’a jamais eu aucun role';
        } else {
            roles = 'a eu comme role : '+roles;
        }
        return roles;
    }

    resume() {
        return this.name+' '+this.getRoles();
    }

    stats() {
        return this.name+' '+this.getRolesHistory();
    }

    
    async incrementRoles() {
        for(let role in this.roles) {
            if (this.roles[role] != null) {
                if(this.roles[role].isMeta) {
                    this.roles[role].timeOut();
                } else {
                    this.roles[role].nb_tours++;
                }
            }
        }
    }
}
class Dieu {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = false;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle de Dieu')
            .setDescription('Le joueur devient Dieu s’il fait un double 4, 5 ou 6 avec les dés normaux.\r\nDouble 4 ou 5 ➡️ S’il y a déjà un Dieu il y a bataille de Dieux (comme un duel de paysan mais toutes les gorgées sont doublées) s’il n’y a pas de Dieu le joueur devient Dieu\r\nDouble 6 ➡️ Pas de bataille, le joueur devient Dieu incontesté et donne 6 gorgées\r\nQuand on devient Dieu on perd les rôles de Catin, Héros, Ecuyer et Princesse.\r\nA chaque fois qu’un joueur joue, si la somme des dés normaux fait 7, Dieu attaque le village et distribue le plus grands des dés normaux (ex : 4 et 3, Dieu distribue 4)');            
        this.game.channel.send(def);
    }

    async duel_dieux(nb) {
        if(nb == null) { nb = 2; }
        if(nb > 2) {
            await this.game.channel.send('Egalitée !!!\r\nLes gorgées en jeu sont maintenant multipliés par '+nb/2);
        } else {
            await this.game.channel.send('DUEL DE DIEUX !!!\r\n<@'+this.game.joueur_actuel.id+'> contre <@'+this.joueur.id+'>');
        }
        let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'>, lance ton dé');
        await sended.react('🎲');
        let filter = (reaction, user) => reaction.emoji.name === '🎲' && user.id === this.game.joueur_actuel.id
        await sended.awaitReactions(filter, {max: 1, time: 120000});
        this.game.joueur_actuel.duel = Math.floor(Math.random() * (6 - 1 +1)) + 1;
        await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> a fait un '+this.game.joueur_actuel.duel);
        if (this.game.joueur_actuel.duel == 3 && this.game.roles.prisonnier.joueur != null) {
            let sended = await this.game.channel.send('Le prisonnier <@'+this.game.roles.prisonnier.joueur.id+'> bois une gorgée');
            await sended.react('🍻');
            let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
            await sended.awaitReactions(filter, {max: 1});
        }
        sended = await this.game.channel.send('<@'+this.joueur.id+'>, à toi de lancer ton dé');
        await sended.react('🎲');
        filter = (reaction, user) => reaction.emoji.name === '🎲' && user.id === this.joueur.id
        await sended.awaitReactions(filter, {max: 1, time: 120000});
        this.joueur.duel = Math.floor(Math.random() * (6 - 1 +1)) + 1;
        await this.game.channel.send('<@'+this.joueur.id+'> a fait un '+this.joueur.duel);
        if (this.joueur.duel == 3 && this.game.roles.prisonnier.joueur != null) {
            let sended = await this.game.channel.send('Le prisonnier <@'+this.game.roles.prisonnier.joueur.id+'> bois une gorgée');
            await sended.react('🍻');
            let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
            await sended.awaitReactions(filter, {max: 1});
        }
        if (this.game.joueur_actuel.duel > this.joueur.duel) {
            let gorgees = (this.game.joueur_actuel.duel - this.joueur.duel) * nb;
            let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> remporte le duel, <@'+this.joueur.id+'> bois '+getDigitEmoji(gorgees)+' gorgée(s)');
            await sended.react('🍻');
            let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
            await sended.awaitReactions(filter, {max: 1});
            return true;
        } else if (this.game.joueur_actuel.duel < this.joueur.duel) {
            let gorgees = (this.joueur.duel - this.game.joueur_actuel.duel) * nb;
            let sended = await this.game.channel.send('<@'+this.joueur.id+'> remporte le duel, <@'+this.game.joueur_actuel.id+'> bois '+getDigitEmoji(gorgees)+' gorgée(s)');
            await sended.react('🍻');
            let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
            await sended.awaitReactions(filter, {max: 1});
            return false;
        } else {
            return await this.duel_dieux(nb*2);
        }
    }

    async attribuer() {
        if((this.joueur == null || this.game.joueur_actuel !== this.joueur) && (this.game.lance_actuel.substr(0,2) == '44' || this.game.lance_actuel.substr(0,2) == '55' || this.game.lance_actuel.substr(0,2) == '66')) {
            if(this.game.roles.demon.joueur == null) {
                let nouveau_dieu = true;
                if(this.joueur != null && (this.game.lance_actuel.substr(0,2) == '44' || this.game.lance_actuel.substr(0,2) == '55')) {
                    nouveau_dieu = await this.duel_dieux();
                }
                if(nouveau_dieu) {
                    if(this.game.joueur_actuel.roles.heros != null) {
                        await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> n’est plus le héros !');
						if(this.game.roles.ecuyer.joueur != null) {
							await this.game.channel.send('<@'+this.game.roles.ecuyer.joueur.id+'> n’est plus l’ecuyer !');
							await this.game.removeRole(this.game.roles.ecuyer.joueur, 'ecuyer');
						}
                    }
                    if(this.game.joueur_actuel.roles.catin != null) {
                        await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> n’est plus la catin !');
                    }
                    if(this.game.joueur_actuel.roles.ecuyer != null) {
                        await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> n’est plus l’ecuyer !');
                    }
                    if(this.game.joueur_actuel.roles.princesse != null) {
                        await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> n’est plus la princesse !');
                    }
                    let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient Dieu !');
                    await sended.react('✅');
                    await sended.react('❔');
                    let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
                    let collected = await sended.awaitReactions(filter, {max: 1});
                    if(collected.first().emoji.name == '❔') { await this.definition(); }
                    await this.game.addRole(this.game.joueur_actuel, 'dieu');
                    await this.game.removeRoles(this.game.joueur_actuel, ['heros','catin','ecuyer','princesse']);
                }
                return true;
            } else {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> tu aurais pu devenir Dieu mais le vil démon <@'+this.game.roles.demon.joueur.id+'> occupe déjà la place');
            }
        }
        return false;
    }
}
class Heros {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = false;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle de Héros')
            .setDescription('Le joueur devient Héros s’il fait double 1, 2 ou 3 avec les dés normaux.\r\nQuand Dieu attaque le village le Héros tente de protéger le village en lançant un dé, s’il fait :\r\n\t    1 ➡️  il est foudroyé, il boit son verre cul-sec et n’est plus le Héros\r\n\t    2 ou 3 ➡️   il échoue et boit le nombre de gorgées en jeu et Dieu distribue les gorgées\r\n\t    4 ou 5 ➡️    le héros réussi seulement à se protéger, Dieu distribue ses gorgées\r\n\t    6 ➡️     le Héros protège le village, Dieu boit les gorgées en jeu');            
        this.game.channel.send(def);
    }
    async attribuer() {       
        if((this.joueur == null || this.game.joueur_actuel !== this.joueur) && (this.game.lance_actuel.substr(0,2) == '11' || this.game.lance_actuel.substr(0,2) == '22' || this.game.lance_actuel.substr(0,2) == '33')) {
            if(this.game.joueur_actuel !== this.game.roles.dieu.joueur) {
                if(this.game.roles.clochard.joueur == null) {
                    if(this.game.roles.ecuyer.joueur != null) {
                        await this.game.channel.send('<@'+this.game.roles.ecuyer.joueur.id+'> n’est plus écuyer !');
                        await this.game.removeRole(this.game.roles.ecuyer.joueur, 'ecuyer');
                    }
                    if(this.game.roles.princesse.joueur === this.game.joueur_actuel) {
                        await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> n’est plus princesse !');
                        await this.game.removeRole(this.game.joueur_actuel, 'princesse');
                    }
                    let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient le héros !');
                    await sended.react('✅');
                    await sended.react('❔');
                    let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
                    let collected = await sended.awaitReactions(filter, {max: 1});
                    if(collected.first().emoji.name == '❔') { await this.definition(); }
                    await this.game.addRole(this.game.joueur_actuel, 'heros');
                    return true;
                } else {
                    await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> aurait pu être un héros mais la place est déjà occupée par le clochard <@'+this.game.roles.clochard.joueur.id+'> !');
                }
            } else {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> on ne peut pas être dieu et héros');
            }
        }
        return false;
    }
}
class Oracle {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = false;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle de l’Oracle')
            .setDescription('Le joueur devient Oracle s’il fait 2 et 1 avec les dés normaux.\r\nTente prédire le score du héros avant que ce dernier ne défende le village. Le score du héros sera orienté vers la prédiction de l’Oracle (ex : si l’Oracle prédit un 6 et que le Héros fait 4, le score devient un 5).\r\nSi sa prédiction est exacte, distribue le nombre de gorgées prédites.');            
        this.game.channel.send(def);
    }

    async attribuer() {
        if((this.joueur == null || this.game.joueur_actuel !== this.joueur) && (this.game.lance_actuel.substr(0,2) == '21')) {
            if(this.game.roles.devin.joueur == null) {
                let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient l’oracle !');
                await sended.react('✅');
                await sended.react('❔');
                let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
                let collected = await sended.awaitReactions(filter, {max: 1});
                if(collected.first().emoji.name == '❔') { await this.definition(); }
                await this.game.addRole(this.game.joueur_actuel, 'oracle');
                return true;
            } else {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> aurait pû devenir oracle mais le devin <@'+this.game.roles.devin.joueur.id+'> est trop fort pour lui...');
            }
        }
        return false;
    }
}
class Ecuyer {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = false;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle de l’Ecuyer')
            .setDescription('Le joueur devient écuyer s’il fait 3 et 1 avec les dés normaux.\r\nBoit tout ce que le Héros boit (autant de gorgées que le Héros).\r\nOn ne peut pas devenir Ecuyer si on est Dieu ou Héros ou s’il n’y a pas de Héros\r\nSi le Héros perd son rôle ou qu’un autre joueur devient Héros, on perd son statut d’Ecuyer\r\nL’Ecuyer peut devenir Héros à la place du Héros.');            
        this.game.channel.send(def);
    }
    async attribuer() {
        if((this.joueur == null || this.game.joueur_actuel !== this.joueur) && (this.game.lance_actuel.substr(0,2) == '31')) {
            if(this.game.roles.apprenti.joueur == null) {
                if(this.game.roles.heros.joueur != null && this.game.roles.heros.joueur !== this.game.joueur_actuel && this.game.roles.dieu.joueur !== this.game.joueur_actuel) {
                    let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient l’ecuyer du héros '+this.game.roles.heros.joueur.name+' !');
                    await sended.react('✅');
                    await sended.react('❔');
                    let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
                    let collected = await sended.awaitReactions(filter, {max: 1});
                    if(collected.first().emoji.name == '❔') { await this.definition(); }
                    await this.game.addRole(this.game.joueur_actuel, 'ecuyer');
                    return true;
                } else {
                    if(this.game.roles.heros.joueur == null) {
                        await this.game.channel.send('<@'+this.game.joueur_actuel.id+'>, tu rêvais de devenir ecuyer mais il n’y a pas de héros à servir...');
                    } else {
                        await this.game.channel.send('<@'+this.game.joueur_actuel.id+'>, tu as failli devenir ecuyer mais tu as un rôle bien plus important à jouer.');
                    }
                }
            } else {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> rêvait de devenir ecuyer mais l’apprenti <@'+this.game.roles.apprenti.joueur.id+'> n’as pas encore fini sa formation ...');
            }
        }
        return false;
    }
}
class Prisonnier {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = false;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle de Prisonnier')
            .setDescription('Le joueur devient Prisonnier s’il fait 3 et 2 avec les dés normaux s’il n’y a pas déjà un Prisonnier, pour fêter son nouveau statut il boit le dé spécial.\r\nOn perd tous ses rôles en rentrant en prison et on ne peut pas en gagner en étant en prison.\r\nBoit quand un joueur fait un 3.\r\nPour sortir de prison il faut faire un 3, pour fêter sa sortie il boit le dé spécial.\r\nSi le Prisonnier fait 3 et 2, il sort et rentre à nouveau en prison, il fête ça deux fois.');            
        this.game.channel.send(def);
    }

    async attribuer() {
        if((this.joueur == null || this.game.joueur_actuel !== this.joueur) && (this.game.lance_actuel.substr(0,2) == '32')) {
            if (this.joueur == null) {
                let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient prisonnier et perds tous ces rôles et bois '+getDigitEmoji(this.game.lance_actuel[2])+' gorgée(s) !');
                await sended.react('✅');
                await sended.react('❔');
                let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
                let collected = await sended.awaitReactions(filter, {max: 1});
                if(collected.first().emoji.name == '❔') { await this.definition(); }
                await this.game.removeRoles(this.game.joueur_actuel, 'all');
                await this.game.addRole(this.game.joueur_actuel, 'prisonnier');
                return true;
            } else {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> à beaucoup de chance que la prison soit pleine !');
            }
        }
        return false;
    }
}
class Catin {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = false;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle de Catin')
            .setDescription('Le joueur devient la Catin s’il fait 4 et 1 avec les dés normaux.\r\nLorsque Dieu attaque le village, la Catin s’interpose en lançant un dé avant le Héros :\r\n\t    Si la Catin fait 1, la Catin sauve le village, Dieu boit ses gorgées et ne distribue pas\r\n\t    Si la Catin fait un autre score, la Catin boit son score\r\nL’Oracle ne prédit pas le score de la Catin.');            
        this.game.channel.send(def);
    }

    async attribuer() {
        if((this.joueur == null || this.game.joueur_actuel !== this.joueur) && (this.game.lance_actuel.substr(0,2) == '41')) {
            if(this.game.joueur_actuel !== this.game.roles.dieu.joueur) {
                if(this.game.roles.gourgandine.joueur == null) {
                    let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient la catin !');
                    await sended.react('✅');
                    await sended.react('❔');
                    let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
                    let collected = await sended.awaitReactions(filter, {max: 1});
                    if(collected.first().emoji.name == '❔') { await this.definition(); }
                    await this.game.addRole(this.game.joueur_actuel, 'catin');
                    return true;
                } else {
                    await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> se sentait l’âme d’une catin mais la gourgandine <@'+this.game.roles.gourgandine.joueur.id+'> ne lui lausse aucun client...');
                }
            } else {
                await this.game.channel.send('Le dieu <@'+this.game.joueur_actuel.id+'> ne saurait être une catin');
            }
        }
        return false;
    }
}
class Aubergiste {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = false;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle d’Aubergiste')
            .setDescription('Le joueur devient Aubergiste s’il fait 5 et 3 avec les dés normaux.\r\nDès que quelqu’un distribue des gorgées à un autres joueur, peut faire +1 ou -1 au nombre de gorgées.\r\nSi les gorgées sont séparées entre différentes personnes, peut faire +1 ou -1 à chaque personne séparément.');            
        this.game.channel.send(def);
    }

    async attribuer() {
        if((this.joueur == null || this.game.joueur_actuel !== this.joueur) && (this.game.lance_actuel.substr(0,2) == '53')) {
            let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient l’aubergiste !');
            await sended.react('✅');
            await sended.react('❔');
            let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
            let collected = await sended.awaitReactions(filter, {max: 1});
            if(collected.first().emoji.name == '❔') { await this.definition(); }
            await this.game.addRole(this.game.joueur_actuel, 'aubergiste');
            return true;
        }
        return false;
    }
}
class Princesse {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = false;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle de Princesse')
            .setDescription('Le joueur devient Princesse s’il fait 5 et 4 avec les dés normaux.\r\nA chaque fois qu’un joueur donne des gorgées à la Princesse, elle peut en redonner la moitié au Héros (ex : si un joueur lui donne 4 gorgées, elle peut en redonner 2 au Héros, donc elle boit 4 et le héros 2).\r\nOn ne peut devenir princesse s’il on est Héros\r\nSi le Héros perd son rôle ou qu’un autre joueur devient Héros, on garde son statut de Princesse.');            
        this.game.channel.send(def);
    }

    async attribuer() {	
        if((this.joueur == null || this.game.joueur_actuel !== this.joueur) && (this.game.lance_actuel.substr(0,2) == '54')) {
            if(this.game.roles.imperatrice.joueur == null) {
                if(this.game.roles.heros.joueur !== this.game.joueur_actuel && this.game.roles.dieu.joueur !== this.game.joueur_actuel) {
                    let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient Princesse !');
                    await sended.react('✅');
                    await sended.react('❔');
                    let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
                    let collected = await sended.awaitReactions(filter, {max: 1});
                    if(collected.first().emoji.name == '❔') { await this.definition(); }
                    await this.game.addRole(this.game.joueur_actuel, 'princesse');
                    return true;
                } else {
                    await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> ne peut pas devenir princesse!');
                }
            } else {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> rêvait d’être une Princesse mais l’Impératrice <@'+this.game.roles.imperatrice.joueur.id+'> s’y opposa...');
            }
        }
        return false;
    }
}
class Dragon {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = false;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle de Dragon')
            .setDescription('Le joueur devient Dragon s’il fait 6 et 5 avec les dés normaux.\r\nLe joueur donne 5 gorgées en plus de devenir Dragon.\r\nQuand un joueur lui donne des gorgées il peut les souffler à droite ou à gauche, il donne son nombre de gorgées divisé par 2 à la première personne à côté de lui, divisé par 4 à la personne d’après, etc jusqu’à arriver à 1.');            
        this.game.channel.send(def);
    }
    async attribuer() {	
        if((this.joueur == null || this.game.joueur_actuel !== this.joueur) && (this.game.lance_actuel.substr(0,2) == '65')) {
            let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient un putain de dragon !!!');
            await sended.react('✅');
            await sended.react('❔');
            let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
            let collected = await sended.awaitReactions(filter, {max: 1});
            if(collected.first().emoji.name == '❔') { await this.definition(); }
            await this.game.addRole(this.game.joueur_actuel, 'dragon');
            return true;
        }
        return false;
    }
}
class Clochard {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = true;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle de clochard')
            .setDescription('Le joueur devient Clochard s’il fait un triple 1.\r\nDevient Héros s’il n’est pas déjà Dieu et ne pourra perdre ce rôle pendant un tour complet des joueurs. De plus pendant ce tour il boira le dé spécial à chaque fois qu’un joueur joue.');            
        this.game.channel.send(def);
    }

    async attribuer() {	
        if(this.game.lance_actuel == '111') {
            if(this.game.joueur_actuel === this.game.roles.dieu.joueur) {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> n’est plus dieu.');
                await this.game.removeRole(this.game.joueur_actuel, 'dieu');
            }
            let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient le heros clochard !!!');
            await sended.react('✅');
            await sended.react('❔');
            let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
            let collected = await sended.awaitReactions(filter, {max: 1});
            if(collected.first().emoji.name == '❔') { await this.definition(); }
            await this.game.addRole(this.game.joueur_actuel, 'clochard');
            await this.game.addRole(this.game.joueur_actuel, 'heros');
            return true;
        }
        return false;
    }

    async timeOut() {
        await this.game.channel.send('Le clochard <@'+this.joueur.id+'> est sorti de la pauvretée mais est toujours le héros!');
        await this.game.removeRole(this.joueur, 'clochard');
    }

}
class Devin {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = true;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle de Devin')
            .setDescription('Le joueur devient Devin s’il fait un triple 2\r\nDevient Oracle et ne pourra perdre ce rôle pendant un tour complet des joueurs. De plus pendant ce tour il faire +1 ou -1 sur n’importe quel dé à chaque fois qu’un joueur joue.');            
        this.game.channel.send(def);
    }

    async attribuer() {	
        if(this.game.lance_actuel == '222') {
            let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient devin !!!');
            await sended.react('✅');
            await sended.react('❔');
            let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
            let collected = await sended.awaitReactions(filter, {max: 1});
            if(collected.first().emoji.name == '❔') { await this.definition(); }
            await this.game.addRole(this.game.joueur_actuel, 'devin');
            await this.game.addRole(this.game.joueur_actuel, 'oracle');
            return true;
        }
        return false;
    }

    async timeOut() {
        await this.game.channel.send('Le devin <@'+this.joueur.id+'> à perdu une partie de ses pouvoirs et n’est plus qu’oracle!');
        await this.game.removeRole(this.joueur, 'devin');
    }
}
class Apprenti {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = true;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle d’Apprenti')
            .setDescription('Le joueur devient Apprenti s’il fait un triple 3.\r\nDevient Ecuyer s’il n’est pas déjà Dieu et qu’il y a déjà un héros autre que lui-même. Il ne pourra perdre ce rôle pendant un tour complet des joueurs. De plus pendant ce tour il boit tout ce que les autres boivent.');
        this.game.channel.send(def);
    }

    async attribuer() {	
        if(this.game.lance_actuel == '333') {
            if(this.game.joueur_actuel === this.game.roles.dieu.joueur) {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> n’est plus dieu.');
                await this.game.removeRole(this.game.joueur_actuel, 'dieu');
            }
            let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient l’apprenti !!!');
            await sended.react('✅');
            await sended.react('❔');
            let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
            let collected = await sended.awaitReactions(filter, {max: 1});
            if(collected.first().emoji.name == '❔') { await this.definition(); }
            await this.game.addRole(this.game.joueur_actuel, 'apprenti');
            await this.game.addRole(this.game.joueur_actuel, 'ecuyer');
            return true;
        }
        return false;
    }

    async timeOut() {
        await this.game.channel.send('L’apprenti <@'+this.joueur.id+'> a fini sa formation et est devenu écuyer !');
        await this.game.removeRole(this.joueur, 'apprenti');
    }
}
class Gourgandine {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = true;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle de Gourgandine')
            .setDescription('Le joueur devient Gourgandine s’il fait un triple 4.\r\nDevient Catin s’il n’est pas déjà Dieu et ne pourra perdre ce rôle pendant un tour complet des joueurs.\r\nDe plus pendant ce tour le Gourgandine s’interposera sur chaque distribution de gorgées (de la même manière que la Catin s’interpose quand Dieu attaque le village).');
        this.game.channel.send(def);
    }

    async attribuer() {	
        if(this.game.lance_actuel == '444') {
            if(this.game.joueur_actuel === this.game.roles.dieu.joueur) {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> n’est plus dieu.');
                await this.game.removeRole(this.game.joueur_actuel, 'dieu');
            }
            let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient la gourgandine !!!');
            await sended.react('✅');
            await sended.react('❔');
            let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
            let collected = await sended.awaitReactions(filter, {max: 1});
            if(collected.first().emoji.name == '❔') { await this.definition(); }
            await this.game.addRole(this.game.joueur_actuel, 'gourgandine');
            await this.game.addRole(this.game.joueur_actuel, 'catin');
            return true;
        }
        return false;
    }

    async timeOut() {
        await this.game.channel.send('La gourgandine <@'+this.joueur.id+'> a mordu son dernier client, elle est redevenue simple catin !');
        await this.game.removeRole(this.joueur, 'gourgandine');
    }
}
class Imperatrice {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = true;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle d’Impératrice')
            .setDescription('Le joueur devient Impératrice s’il fait un triple 5\r\nDevient Princesse s’il n’est pas Héros et ne pourra perdre ce rôle pendant un tour complet des joueurs.\r\nDe plus pendant ce tour il peut rediriger la moitié de ses gorgées vers n’importe qui (et donc ne boire que la moitié des gorgées qu’on lui donne).');
        this.game.channel.send(def);
    }

    async attribuer() {	
        if(this.game.lance_actuel == '555') {
            if(this.game.joueur_actuel === this.game.roles.dieu.joueur) {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> n’est plus dieu.');
                await this.game.removeRole(this.game.joueur_actuel, 'dieu');
            }
            let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> devient Impératrice !!!');
            await sended.react('✅');
            await sended.react('❔');
            let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
            let collected = await sended.awaitReactions(filter, {max: 1});
            if(collected.first().emoji.name == '❔') { await this.definition(); }
            await this.game.addRole(this.game.joueur_actuel, 'imperatrice');
            await this.game.addRole(this.game.joueur_actuel, 'princesse');
            return true;
        }
        return false;
    }

    async timeOut() {
        await this.game.channel.send('L’Impératrice <@'+this.joueur.id+'> à perdu de son influence et n’est plus qu’une banale princesse!');
        await this.game.removeRole(this.joueur, 'imperatrice');
    }
}
class Demon {
    constructor(Game, Player) {
        this.joueur = Player;
        this.nb_tours = 0;
        this.game = Game;
        this.isMeta = true;
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Rôle de Démon')
            .setDescription('Le joueur devient Démon s’il fait un triple 6.\r\nDevient Dieu et ne pourra perdre ce rôle pendant un tour complet des joueurs.\r\nDe plus pendant ce tour il distribuera le dé spécial à chaque fois qu’un joueur joue.');            
        this.game.channel.send(def);
    }

    async attribuer() {	
        if(this.game.lance_actuel == '666') {
            if(this.game.joueur_actuel.roles.heros != null) {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> n’est plus le héros !');
            }
            if(this.game.joueur_actuel.roles.catin != null) {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> n’est plus la catin !');
            }
            if(this.game.joueur_actuel.roles.ecuyer != null) {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> n’est plus l’ecuyer !');
            }
            if(this.game.joueur_actuel.roles.princesse != null) {
                await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> n’est plus la princesse !');
            }
            let sended = await this.game.channel.send('<@'+this.game.joueur_actuel.id+'> va vous prouver que Dieu n’est que le reflet du diable dans le ciel en devenant le démon !!!');
            await sended.react('✅');
            await sended.react('❔');
            let filter = (reaction, user) => this.game.getPlayersId().includes(user.id)
            let collected = await sended.awaitReactions(filter, {max: 1});
            if(collected.first().emoji.name == '❔') { await this.definition(); }
            await this.game.addRole(this.game.joueur_actuel, 'demon');
            await this.game.addRole(this.game.joueur_actuel, 'dieu');
            await this.game.removeRoles(this.game.joueur_actuel, ['heros','catin','ecuyer','princesse']);
            return true;
        }
        return false;
    }

    async timeOut() {
        await this.game.channel.send('Le démon <@'+this.joueur.id+'> à perdu de sa toute puissance mais reste votre dieu!');
        await this.game.removeRole(this.joueur, 'demon');
    }
}
class Game {
    constructor(chan) {
        this.channel = chan;
        this.joueurs = new Array();
        this.tour = 0;
        this.aqui = 0;
        this.des_pipes = '000'; // pour débug uniquement
        this.lance_actuel = '000';
        this.lance_precedent = '000';
        this.joueur_precedent = new Player(0, '');
        this.joueur_actuel = new Player(0, '');
        this.duel_paysans_vainqueur = true;
        this.commence = null;
        this.reglesId = null;
        this.newGameId = null;
        this.roles = {clochard: new Clochard(this), devin: new Devin(this), apprenti: new Apprenti(this), gourgandine: new Gourgandine(this), imperatrice: new Imperatrice(this), demon: new Demon(this), dieu: new Dieu(this), heros: new Heros(this), oracle: new Oracle(this), ecuyer: new Ecuyer(this), prisonnier: new Prisonnier(this), catin: new Catin(this), aubergiste: new Aubergiste(this), princesse: new Princesse(this), dragon: new Dragon(this)};
    }

    definition() {
        let def = new Discord.MessageEmbed()
            .setColor(0xffffff)
            .setTitle('Règles générales')
            .setDescription('Dans ce jeu, des rôles vous seront affectés en fonction de vos lancés de dés. pour voir le détail de ces rôles, cliquez sur ❔\r\n6 sur un dé normal => distribue l’autre dé normal (ex : 6 et 2 : distribue 2)\r\n51/42 sur les dés normaux : tout le monde boit le dé spécial\r\nDeux joueurs consécutifs font exactement le même score doivent faire un duel de paysan :\r\n\t    Chaque joueur lance un dé, le perdant boit la différence. En cas d’égalité, les deux joueurs relancent mais les gorgées en jeu sont doublés.');
        this.channel.send(def).then(sended => {
            this.reglesId = sended.id;
            sended.react('❔');
        }).catch(function (error) {
            console.log(error);
        });;
    }

    getPlayersId() {
        let ids = new Array();
        this.joueurs.forEach(function (joueur) {
            ids.push(joueur.id);
        });
        return ids;
    }

    showRoles() {
        var j = new Player(this,null, null);
        for (let role in j.roles) {
            this.roles[role].definition();
            this.reglesId = null;
        }
    }

    addPlayer(id, name) {
        if(this.commence != null) {
            if(this.getPlayersId().includes(id)) {
                this.channel.send('<@'+id+'> est déjà dans la partie !');
            } else {
                this.joueurs.push(new Player(this, id, name));
                this.channel.send('<@'+id+'> a rejoint la partie !');
            }
        } else {
            this.channel.send('La partie n’a pas encore commencé, utilisez la commande commencer pour débuter une nouvelle partie !');
        }
        
    }
    
    addRole(Player, role) {
        this.joueurs.forEach(function (joueur) {
            joueur.roles[role] = null;
        });
        eval('Player.roles[role] = new '+role[0].toUpperCase()+role.slice(1)+'(this, Player);');
        this.roles[role] = Player.roles[role];
        Player.rolesHistory[role]++;
    }
    removeRole(Player, role) {
        if(Player.roles[role] != null) {
            Player.roles[role] = null;
            eval('this.roles[role] = new '+role[0].toUpperCase()+role.slice(1)+'(this);');
        }
    }
    removeRoles(Player, roles_list) {
        if (Array.isArray(roles_list)) {
            roles_list.forEach(function (role) {
                Player.game.removeRole(Player, role);
            });
        } else if (roles_list === 'all')  {
            for (let role in Player.game.roles) {
                Player.game.removeRole(Player, role);
            }
        } else {
            Player.game.removeRole(roles);
        }
        
    }
    async removePlayer(removed_id, by_id) {
        var removed = null;
        var by = null;
        this.joueurs.forEach(function (joueur) {
            if(joueur.id == removed_id) {
                removed = joueur;
            }
            if(joueur.id == by_id) {
                by = joueur;
            }
        });
        var game = this;
        if(this.joueurs.includes(removed) && (by == null || by === removed || this.joueurs.includes(by))) {
            let confirmed = true;
            if(this.joueurs.includes(by) && by !== removed) {
                let sended = await this.channel.send('<@'+removed.id+'>, '+by.name+' souhaite vous exclure de la partie.\r\nVous avez 30 secondes pour répondre, souhaitez vous quitter la partie ?');
                await sended.react('✅');
                await sended.react('❌');
                let filter = (reaction, user) => removed.id == user.id
                let collected = await sended.awaitReactions(filter, {max: 1, time: 30000});
                if(collected.first() != null && collected.first().emoji.name == '❌') { confirmed = false; }
            }
            if(by === removed) {
                let sended = await this.channel.send('<@'+removed.id+'>, vous avez 30 secondes pour répondre.\r\nSouhaitez vous quitter la partie ?');
                await sended.react('✅');
                await sended.react('❌');
                let filter = (reaction, user) => removed.id == user.id
                let collected = await sended.awaitReactions(filter, {max: 1, time: 30000});
                if(collected.first() != null && collected.first().emoji.name == '❌') { confirmed = false; }
            }
            if(confirmed) {
                let joueurs = new Array();
                for(var i = 0; i < game.joueurs.length; i++) {
                    if(game.joueurs[i] !== removed) {
                        joueurs.push(game.joueurs[i]);
                    } else {
                        if(game.commence != null) {
                            game.channel.send('<@'+removed.id+'> a quitté la partie !')
                        }
                        this.removeRoles(removed, 'all');
                        var place = i;
                    }
                }
                if(place < game.aqui) {
                    game.aqui--;
                }
                this.joueurs = joueurs;
                return true;
            } else {
                return false
            }
        }
    }

    resume() {
        if (this.commence != null) {
            let resume_joueurs = '';
            this.joueurs.forEach(function (joueur) {
                resume_joueurs = resume_joueurs+'\r\n'+joueur.resume();
            });
            let resume = new Discord.MessageEmbed()
                .setColor(0xffffff)
                .setTitle('Résumé du jeu')
                .setDescription(resume_joueurs);
            this.channel.send(resume);
        } else {
            this.channel.send('La partie n’a pas encore commencé, utilisez la commande commencer pour débuter une nouvelle partie !');
        }
    }

    stats() {
        if (this.commence != null) {
            let stats_joueurs = '';
            this.joueurs.forEach(function (joueur) {
                stats_joueurs = stats_joueurs+'\r\n'+joueur.stats();
            });
            let stats = new Discord.MessageEmbed()
                .setColor(0xffffff)
                .setTitle('Stats du jeu')
                .setDescription(stats_joueurs);
            this.channel.send(stats);
        } else {
            this.channel.send('La partie n’a pas encore commencé, utilisez la commande commencer pour débuter une nouvelle partie !');
        }
    }

    async suivant() {
        if (this.joueurs.length > 1) {
            this.aqui++;
            if (this.aqui >= this.joueurs.length) {
                this.aqui = 0;
                this.tour++;
            }
            return await this.jouer();
        } else {
            this.channel.send('Fin de la partie : il n’y a plus assès de joueurs');
            return false;
        }
    }

    async distribue_gorgees(Player, i, text) {
        if (this.roles.gourgandine.joueur != null) {
            let sended = await Player.game.channel.send('<@'+Player.id+'> veux distribuer '+getDigitEmoji(i)+' gorgée(s) mais la gourgandine <@'+this.roles.gourgandine.joueur.id+'> s’interpose !!!\r\nSi la gourgandine a quittée le jeu cliquez sur la porte pour l’exclure');
            await sended.react('🎲');
            await sended.react('🚪');
            let filter = (reaction, user) => Player.game.getPlayersId().includes(user.id) && ((reaction.emoji.name === '🎲' && user.id === this.roles.gourgandine.joueur.id) || reaction.emoji.name === '🚪')
            let collected = await sended.awaitReactions(filter, {max: 1});
            if (collected.first().emoji.name !== '🚪' || !await this.removePlayer(Player.game.roles.catin.joueur.id, collected.first().users.cache.last())) {
                let de = Math.floor(Math.random() * (6 - 1 +1)) + 1;
                if (de == 1) {
                    await Player.game.channel.send('La gourgandine <@'+Player.game.roles.gourgandine.joueur.id+'> a fait '+getDigitEmoji(1)+' et sauve ses compagnons !');
                    let sended = await Player.game.channel.send('<@'+Player.id+'> boit '+getDigitEmoji(i)+' gorgée(s) !');
                    await sended.react('🍻');
                    let filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                    await sended.awaitReactions(filter, {max: 1});
                    return true;
                } else {
                    let sended = await Player.game.channel.send('La gourgandine <@'+Player.game.roles.gourgandine.joueur.id+'> a fait '+getDigitEmoji(de)+' et echoue dans sa tentative de séduction de '+Player.name+'\r\nElle boit '+getDigitEmoji(de)+' gorgées !');
                    await sended.react('🍻');
                    let filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                    await sended.awaitReactions(filter, {max: 1});
                    if (de == 3 && this.roles.prisonnier.joueur != null) {
                        let sended = await Player.game.channel.send('Le prisonnier <@'+this.roles.prisonnier.joueur.id+'> bois une gorgée');
                        await sended.react('🍻');
                        let filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                        await sended.awaitReactions(filter, {max: 1});
                    }
                }
            }
        }
        if(text == null) {
            await this.channel.send('<@'+Player.id+'> distribue '+getDigitEmoji(i)+' gorgée(s) !');
        } else {
            await this.channel.send(text);
        }
        
        if (this.roles.aubergiste.joueur != null) {
            await this.channel.send('L’aubergiste <@'+this.roles.aubergiste.joueur.id+'> peut, à sa convenance augmenter ou diminuer de 1 le nombre de gorgées pour chaque personne désignée');
        }
        if (this.roles.apprenti.joueur != null) {
            await this.channel.send('Apprenti <@'+this.roles.apprenti.joueur.id+'> n’oublie pas que tu dois boire la même quantité que chaque personnes qui bois (bon courage!)');
        }
        if (this.roles.imperatrice.joueur != null) {
            await this.channel.send('Impératrice <@'+this.roles.imperatrice.joueur.id+'>, si tu reçoit des gorgées tu peux en boire que la moitié et redistribuer l’autre moitiée à qui tu veux');
        } else if (this.roles.princesse.joueur != null && this.roles.heros.joueur != null) {
            await this.channel.send('Princesse <@'+this.roles.princesse.joueur.id+'>, si tu reçoit des gorgées, tu dois toutes les boires mais tu peux distribuer la moitiée à notre héros <@'+this.roles.heros.joueur.id+'> !');
        }
        if (this.roles.dragon.joueur != null) {
            let ordre = '';
            this.joueurs.forEach(function (joueur) {
                ordre = ordre + ' - '+joueur.name;
            });
            ordre = ordre.slice(3);
            await this.channel.send('Si le dragon <@'+this.roles.dragon.joueur.id+'> reçoit des gorgées, il peut souffler à droite ou à gauche, il donne son nombre de gorgées divisé par 2 à la première personne à côté de lui, divisé par 4 à la personne d’après, etc jusqu’à arriver à 1.\r\nrappel de l’ordre des joueurs : '+ordre);
        }
        let sended = await this.channel.send('Cliquez sur la choppe quand les gorgées ont été bues.');
        await sended.react('🍻');
        let filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
        await sended.awaitReactions(filter, {max: 1});
    }

    async lancer_des(Player) {
        let sended = await this.channel.send('Au tour de <@'+Player.id+'> de lancer les dés! (clique sur le dé)\r\n Si '+Player.name+' n’est plus là cliquez sur la porte pour l’exclure du jeu');
        await sended.react('🎲');
        await sended.react('🚪');
        const filter = (reaction, user) => Player.game.getPlayersId().includes(user.id) && ((reaction.emoji.name === '🎲' && user.id === Player.id) || reaction.emoji.name === '🚪')
        let collected = await sended.awaitReactions(filter, {max: 1});
        if (collected.first().emoji.name !== '🚪' || !await this.removePlayer(Player.id, collected.first().users.cache.last())) {
            if(this.des_pipes == '000') { // Pour débug uniquement
                var de1 = Math.floor(Math.random() * (6 - 1 +1)) + 1;
                var de2 = Math.floor(Math.random() * (6 - 1 +1)) + 1;
                var de3 = Math.floor(Math.random() * (6 - 1 +1)) + 1;
            } else {
                var de1 = parseInt(this.des_pipes[0],10);
                var de2 = parseInt(this.des_pipes[1],10);
                var de3 = parseInt(this.des_pipes[2],10);
            }
            this.lance_precedent = this.lance_actuel;
            this.joueur_precedent = this.joueur_actuel;
            this.joueur_actuel = Player;
            await Player.incrementRoles();
            if (de1>de2) {
                this.lance_actuel = de1+''+de2+''+de3;
            } else {
                this.lance_actuel = de2+''+de1+''+de3;
            }
            await this.channel.send('<@'+Player.id+'> a fait '+getDigitEmoji(de1)+' et '+getDigitEmoji(de2)+' avec les dés normaux et '+getDigitEmoji(de3)+' avec le dé spécial');
            return true;
        } else {
            return await this.suivant();
        }
    }

    async duel_paysans(Player, nb) {
        if(nb == null) {nb = 1;}
        if (this.getPlayersId().includes(this.joueur_precedent.id) && this.lance_actuel.substr(0,2) === this.lance_precedent.substr(0,2)) {
            if (nb > 1) {
                await this.channel.send('Egalitée !!!\r\nLes gorgées en jeu sont maintenant multipliés par '+nb);
            } else {
                await this.channel.send('DUEL DE PAYSANS !!!\r\n<@'+Player.id+'> contre <@'+Player.game.joueur_precedent.id+'>');
            }
            let sended = await this.channel.send('<@'+Player.id+'>, lance ton dé');
            await sended.react('🎲');
            let filter = (reaction, user) => reaction.emoji.name === '🎲' && user.id === Player.id
            await sended.awaitReactions(filter, {max: 1, time: 120000});
            Player.duel = Math.floor(Math.random() * (6 - 1 +1)) + 1;
            if (Player.duel == 3 && this.roles.prisonnier.joueur != null) {
                await this.channel.send('Le prisonnier <@'+this.roles.prisonnier.joueur.id+'> bois une gorgée');
            }
            await this.channel.send('<@'+Player.id+'> a fait un '+getDigitEmoji(Player.duel));
            sended = await this.channel.send('<@'+Player.game.joueur_precedent.id+'>, à toi de lancer ton dé');
            await sended.react('🎲');
            filter = (reaction, user) => reaction.emoji.name === '🎲' && user.id === Player.game.joueur_precedent.id
            await sended.awaitReactions(filter, {max: 1, time: 120000});
            Player.game.joueur_precedent.duel = Math.floor(Math.random() * (6 - 1 +1)) + 1;
            if (Player.game.joueur_precedent.duel == 3 && this.roles.prisonnier.joueur != null) {
                await this.channel.send('Le prisonnier <@'+this.roles.prisonnier.joueur.id+'> bois une gorgée');
            }
            await this.channel.send('<@'+Player.game.joueur_precedent.id+'> a fait un '+getDigitEmoji(Player.game.joueur_precedent.duel));
            if (Player.duel > Player.game.joueur_precedent.duel) {
                this.duel_paysans_vainqueur = true;
                let gorgees = (Player.duel - Player.game.joueur_precedent.duel) * nb;
                Player.game.joueur_precedent.gorgees = Player.game.joueur_precedent.gorgees + gorgees
                let sended = await this.channel.send('<@'+Player.id+'> remporte le duel, <@'+Player.game.joueur_precedent.id+'> bois '+getDigitEmoji(gorgees)+' gorgées');
                await sended.react('🍻');
                let filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                await sended.awaitReactions(filter, {max: 1});
            } else if (Player.duel < Player.game.joueur_precedent.duel) {
                this.duel_paysans_vainqueur = false;
                let gorgees = (Player.game.joueur_precedent.duel - Player.duel) * nb;
                Player.gorgees = Player.gorgees + gorgees;
                let sended = await this.channel.send('<@'+Player.game.joueur_precedent.id+'> remporte le duel, il garde son role et <@'+Player.id+'> bois '+getDigitEmoji(gorgees)+' gorgées');
                await sended.react('🍻');
                let filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                await sended.awaitReactions(filter, {max: 1});
            } else {
                return await this.duel_paysans(Player, nb*2);
            }
            return true;
        } else {
            return false;
        }
    }

    async tout_le_monde_bois() {
        if (this.lance_actuel.substr(0,2) == '51' || this.lance_actuel.substr(0,2) == '42') {
            let gorgees = parseInt(this.lance_actuel.slice(2), 10);
            this.joueurs.forEach(function (joueur) {
                joueur.gorgees = joueur.gorgees + gorgees;
            });
            let sended = await this.channel.send(getDigitEmoji(parseInt(this.lance_actuel.substr(0,1), 10))+getDigitEmoji(parseInt(this.lance_actuel.substr(1,1), 10))+'!!! Tout le monde boit '+getDigitEmoji(gorgees)+' gorgées !!!');
            await sended.react('🍻');
            let filter = (reaction, user) => this.getPlayersId().includes(user.id)
            await sended.awaitReactions(filter, {max: 1});
            return true;
        } else {
            return false;
        }
    }

    async dieu_attaque(Player) {
        var sended = null;
        var filter = null;
        var collected = null;
        var de = 0;
        var prediction = 0;
        if (parseInt(this.lance_actuel[0], 10) + parseInt(this.lance_actuel[1], 10) == 7 && this.roles.dieu.joueur != null) {
            await this.channel.send('Le Dieu <@'+this.roles.dieu.joueur.id+'> attaque le village !!!');
            if (this.roles.catin.joueur != null) {
                sended = await this.channel.send('La catin <@'+this.roles.catin.joueur.id+'> s’interpose !!!\r\nSi la catin a quittée le jeu cliquez sur la porte pour l’exclure');
                await sended.react('🎲');
                await sended.react('🚪');
                filter = (reaction, user) => Player.game.getPlayersId().includes(user.id) && ((reaction.emoji.name === '🎲' && user.id === Player.game.roles.catin.joueur.id) || reaction.emoji.name === '🚪')
                collected = await sended.awaitReactions(filter, {max: 1});
                if (collected.first().emoji.name !== '🚪' || !await this.removePlayer(Player.game.roles.catin.joueur.id, collected.first().users.cache.last())) {
                    de = Math.floor(Math.random() * (6 - 1 +1)) + 1;
                    if (de == 1) {
                        await this.channel.send('La catin <@'+this.roles.catin.joueur.id+'> a fait '+getDigitEmoji(1)+' et sauve miraculeusement le village !');
                        sended = await this.channel.send('Le Dieu <@'+this.roles.dieu.joueur.id+'> boit '+getDigitEmoji(this.lance_actuel[0])+' gorgées !');
                        await sended.react('🍻');
                        filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                        await sended.awaitReactions(filter, {max: 1});
                        return true;
                    } else {
                        sended = await this.channel.send('La catin <@'+this.roles.catin.joueur.id+'> a fait '+getDigitEmoji(de)+' et echoue dans sa tentative de séduction de Dieu\r\nElle boit '+getDigitEmoji(de)+' gorgées !');
                        await sended.react('🍻');
                        filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                        await sended.awaitReactions(filter, {max: 1});
                    }
                    if (de == 3 && this.roles.prisonnier.joueur != null) {
                        sended = await this.channel.send('Le prisonnier <@'+this.roles.prisonnier.joueur.id+'> bois une gorgée');
                        await sended.react('🍻');
                        filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                        await sended.awaitReactions(filter, {max: 1});
                    }
                }
            }
            if (this.roles.oracle.joueur != null && this.roles.heros.joueur != null) {
                while(true) {
                    sended = await this.channel.send('L’Oracle <@'+this.roles.oracle.joueur.id+'> tente de prédire la lancé de dé du héros...\r\nSi l’Oracle a quitté le jeu cliquez sur la porte pour l’exclure');
                    await sended.react(getDigitEmoji(1));
                    await sended.react(getDigitEmoji(2));
                    await sended.react(getDigitEmoji(3));
                    await sended.react(getDigitEmoji(4));
                    await sended.react(getDigitEmoji(5));
                    await sended.react(getDigitEmoji(6));
                    await sended.react('🚪');
                    filter = (reaction, user) => Player.game.getPlayersId().includes(user.id) && (((reaction.emoji.name === getDigitEmoji(1) || reaction.emoji.name === getDigitEmoji(2) || reaction.emoji.name === getDigitEmoji(3) || reaction.emoji.name === getDigitEmoji(4) || reaction.emoji.name === getDigitEmoji(5) || reaction.emoji.name === getDigitEmoji(6)) && user.id === Player.game.roles.oracle.joueur.id) || reaction.emoji.name === '🚪')
                    collected = await sended.awaitReactions(filter, {max: 1});
                    switch(collected.first().emoji.name) {
                        case getDigitEmoji(1):
                            prediction = 1;
                            break;
                        case getDigitEmoji(2):
                            prediction = 2;
                            break;
                        case getDigitEmoji(3):
                            prediction = 3;
                            break;
                        case getDigitEmoji(4):
                            prediction = 4;
                            break;
                        case getDigitEmoji(5):
                            prediction = 5;
                            break;
                        case getDigitEmoji(6):
                            prediction = 6;
                            break;                            
                    }
                    if(prediction != 0 || (collected.first().emoji.name == '🚪' && await this.removePlayer(Player.game.roles.oracle.joueur.id, collected.first().users.cache.last()))) {
                        break;
                    }
                }
            }
            if (this.roles.heros.joueur != null) {
                sended = await this.channel.send('Le héros <@'+this.roles.heros.joueur.id+'> tente de protéger le village !!!\r\nSi le héros a quitté le jeu cliquez sur la porte pour l’exclure');
                await sended.react('🎲');
                await sended.react('🚪');
                filter = (reaction, user) => Player.game.getPlayersId().includes(user.id) && ((reaction.emoji.name === '🎲' && user.id === Player.game.roles.heros.joueur.id) || reaction.emoji.name === '🚪')
                collected = await sended.awaitReactions(filter, {max: 1});
                if (collected.first().emoji.name !== '🚪' || !await this.removePlayer(Player.game.roles.heros.joueur.id, collected.first().users.cache.last())) {
                    de = Math.floor(Math.random() * (6 - 1 +1)) + 1;
                    await this.channel.send('Le héros <@'+this.roles.heros.joueur.id+'> a fait '+getDigitEmoji(de));
                    if (de == 3 && this.roles.prisonnier.joueur != null) {
                        sended = await this.channel.send('Le prisonnier <@'+this.roles.prisonnier.joueur.id+'> bois une gorgée');
                        await sended.react('🍻');
                        filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                        await sended.awaitReactions(filter, {max: 1});
                    }
                    if (prediction !== 0) {
                        if(prediction == de) {
                            await this.distribue_gorgees(this.roles.oracle.joueur, de, 'La prédiction de l’Oracle <@'+this.roles.oracle.joueur.id+'> était exacte, il distribue '+getDigitEmoji(de)+' gorgée(s) !');
                        } else if(prediction < de) {
                            de--;
                            await this.channel.send('La prédiction de l’Oracle <@'+this.roles.oracle.joueur.id+'> était inférieure au lancé du héros, le score du héros est maintenant de '+getDigitEmoji(de));
                        } else {
                            de++;
                            await this.channel.send('La prédiction de l’Oracle <@'+this.roles.oracle.joueur.id+'> était supérieure au lancé du héros, le score du héros est maintenant de '+getDigitEmoji(de));
                        }
                    }
                    if(de == 1) {
                        sended = await this.channel.send('Le héros <@'+this.roles.heros.joueur.id+'> est foudroyé sur place, il boit son verre sec en dernier geste d’héroïsme...');
                        await sended.react('🍻');
                        filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                        await sended.awaitReactions(filter, {max: 1});
                        this.removeRole(this.roles.heros.joueur, 'heros');
                        if (this.roles.ecuyer.joueur != null) {
                            sended = await this.channel.send('L’ecuyer <@'+this.roles.ecuyer.joueur.id+'> suit l’exemple, il boit son verre sec avant de déposes lui aussi les armes...');
                            await sended.react('🍻');
                            filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                            await sended.awaitReactions(filter, {max: 1});
                            this.removeRole(this.roles.ecuyer.joueur, 'ecuyer');
                        }
                    } else if (de == 2 || de == 3) {
                        sended = await this.channel.send('Le héros <@'+this.roles.heros.joueur.id+'> échoue, il boit '+getDigitEmoji(this.lance_actuel[0])+' gorgées');
                        await sended.react('🍻');
                        filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                        await sended.awaitReactions(filter, {max: 1});
                        if (this.roles.ecuyer.joueur != null) {
                            sended = await this.channel.send('L’ecuyer <@'+this.roles.ecuyer.joueur.id+'> suis l’exemple, il boit '+getDigitEmoji(this.lance_actuel[0])+' gorgées');
                            await sended.react('🍻');
                            filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                            await sended.awaitReactions(filter, {max: 1});
                        }
                    } else if (de == 4 || de == 5) {
                        await this.channel.send('Le héros <@'+this.roles.heros.joueur.id+'> réussit seulement a se protéger lui-même');
                    } else {
                        await this.channel.send('Le héros <@'+this.roles.heros.joueur.id+'> réussit à protéger tout le village !!!');
                        sended = await this.channel.send('Le Dieu <@'+this.roles.dieu.joueur.id+'> boit '+getDigitEmoji(this.lance_actuel[0])+' gorgées !');
                        await sended.react('🍻');
                        filter = (reaction, user) => Player.game.getPlayersId().includes(user.id)
                        await sended.awaitReactions(filter, {max: 1});
                        return true;
                    }
                }
            }
            await this.distribue_gorgees(this.roles.dieu.joueur, this.lance_actuel[0], 'Le Dieu <@'+this.roles.dieu.joueur.id+'> distribue '+getDigitEmoji(this.lance_actuel[0])+' gorgées !');
            return true;
        } else {
            if(parseInt(this.lance_actuel[0], 10) + parseInt(this.lance_actuel[1], 10) == 7) {
                await this.channel.send('Les dieux sont calmes ...');
            }
            return false;
        }
    }

    async devin_influence() {
        if (this.roles.devin.joueur != null) { 
            let influence = new Array(this.lance_actuel);
            let choix = '\r\n\r\nClique sur '+getDigitEmoji(1)+' pour ne pas modifier le lancé de dés.';
            let val = ''
            if(parseInt(this.lance_actuel[0], 10)-1 > 0 && parseInt(this.lance_actuel[0], 10)-1 < 7) {
                if (parseInt(this.lance_actuel[0], 10)-1 > parseInt(this.lance_actuel[1], 10)) {
                    val = (parseInt(this.lance_actuel[0], 10)-1)+this.lance_actuel[1]+this.lance_actuel[2];
                } else {
                    val = this.lance_actuel[1]+(parseInt(this.lance_actuel[0], 10)-1)+this.lance_actuel[2];
                }
                if (!influence.includes(val)) { influence.push(val); }
            }
            if(parseInt(this.lance_actuel[0], 10)+1 > 0 && parseInt(this.lance_actuel[0], 10)+1 < 7) {
                if (parseInt(this.lance_actuel[0], 10)+1 > parseInt(this.lance_actuel[1], 10)) {
                    val = (parseInt(this.lance_actuel[0], 10)+1)+this.lance_actuel[1]+this.lance_actuel[2];
                } else {
                    val = this.lance_actuel[1]+(parseInt(this.lance_actuel[0], 10)+1)+this.lance_actuel[2];
                }
                if (!influence.includes(val)) { influence.push(val); }
            }
            if(parseInt(this.lance_actuel[1], 10)-1 > 0 && parseInt(this.lance_actuel[1], 10)-1 < 7) {
                if (parseInt(this.lance_actuel[1], 10)-1 > parseInt(this.lance_actuel[0], 10)) {
                    val = (parseInt(this.lance_actuel[1], 10)-1)+this.lance_actuel[0]+this.lance_actuel[2];
                } else {
                    val = this.lance_actuel[0]+(parseInt(this.lance_actuel[1], 10)-1)+this.lance_actuel[2];
                }
                if (!influence.includes(val)) { influence.push(val); }
            }
            if(parseInt(this.lance_actuel[1], 10)+1 > 0 && parseInt(this.lance_actuel[1], 10)+1 < 7) {
                if (parseInt(this.lance_actuel[1], 10)+1 > parseInt(this.lance_actuel[0], 10)) {
                    val = (parseInt(this.lance_actuel[1], 10)+1)+this.lance_actuel[0]+this.lance_actuel[2];
                } else {
                    val = this.lance_actuel[0]+(parseInt(this.lance_actuel[1], 10)+1)+this.lance_actuel[2];
                }
                if (!influence.includes(val)) { influence.push(val); }
            }
            if(parseInt(this.lance_actuel[2], 10)-1 > 0 && parseInt(this.lance_actuel[2], 10)-1 < 7) {
                val = this.lance_actuel[0]+this.lance_actuel[1]+(parseInt(this.lance_actuel[2], 10)-1);
                if (!influence.includes(val)) { influence.push(val); }
            }
            if(parseInt(this.lance_actuel[2], 10)+1 > 0 && parseInt(this.lance_actuel[2], 10)+1 < 7) {
                val = this.lance_actuel[0]+this.lance_actuel[1]+(parseInt(this.lance_actuel[2], 10)+1);
                if (!influence.includes(val)) { influence.push(val); }
            }
            for(let i = 1; i < influence.length; i++) {
                choix = choix + '\r\nClique sur '+getDigitEmoji(i+1)+' pour changer le lancé de dés en '+getDigitEmoji(influence[i][0])+' et '+getDigitEmoji(influence[i][1])+' avec les dés normaux et '+getDigitEmoji(influence[i][2])+' avec le dé spécial.';
            }
            while(true) {
                let devin = false;
                let sended = await this.channel.send('Le devin <@'+this.roles.devin.joueur.id+'> peut influencer un des dés jetté.'+choix+'\r\nSi le devin a quitté le jeu cliquez sur la porte pour l’exclure');
                for(let i = 0; i < influence.length; i++) {
                    await sended.react(getDigitEmoji(i+1));
                }
                sended.react('🚪');
                let filter = (reaction, user) => this.getPlayersId().includes(user.id) && (((reaction.emoji.name === getDigitEmoji(1) || reaction.emoji.name === getDigitEmoji(2) || reaction.emoji.name === getDigitEmoji(3) || reaction.emoji.name === getDigitEmoji(4) || reaction.emoji.name === getDigitEmoji(5) || reaction.emoji.name === getDigitEmoji(6) || reaction.emoji.name === getDigitEmoji(7)) && user.id === this.roles.devin.joueur.id) || reaction.emoji.name === '🚪')
                let collected = await sended.awaitReactions(filter, {max: 1});
                switch(collected.first().emoji.name) {
                    case getDigitEmoji(1):
                        devin = true;
                        await this.channel.send('Le lancé de dés n’a pas été modifié par le devin.');
                        break;
                    case getDigitEmoji(2):
                        devin = true;
                        if(influence.length > 1) {
                            this.lance_actuel = influence[1];
                            await this.channel.send('Les dés affichent maintenant '+this.lance_actuel[0]+' et '+this.lance_actuel[1]+' avec les dés normaux et '+this.lance_actuel[2]+' avec le dé spécial');
                        } else {
                            await this.channel.send('Le lancé de dés n’a pas été modifié par le devin.');
                        }
                        break;
                    case getDigitEmoji(3):
                        devin = true;
                        if(influence.length > 2) {
                            this.lance_actuel = influence[2];
                            await this.channel.send('Les dés affichent maintenant '+this.lance_actuel[0]+' et '+this.lance_actuel[1]+' avec les dés normaux et '+this.lance_actuel[2]+' avec le dé spécial');
                        } else {
                            await this.channel.send('Le lancé de dés n’a pas été modifié par le devin.');
                        }
                        break;
                    case getDigitEmoji(4):
                        devin = true;
                        if(influence.length > 3) {
                            this.lance_actuel = influence[3];
                            await this.channel.send('Les dés affichent maintenant '+this.lance_actuel[0]+' et '+this.lance_actuel[1]+' avec les dés normaux et '+this.lance_actuel[2]+' avec le dé spécial');
                        } else {
                            await this.channel.send('Le lancé de dés n’a pas été modifié par le devin.');
                        }
                        break;
                    case getDigitEmoji(5):
                        devin = true;
                        if(influence.length > 4) {
                            this.lance_actuel = influence[4];
                            await this.channel.send('Les dés affichent maintenant '+this.lance_actuel[0]+' et '+this.lance_actuel[1]+' avec les dés normaux et '+this.lance_actuel[2]+' avec le dé spécial');
                        } else {
                            await this.channel.send('Le lancé de dés n’a pas été modifié par le devin.');
                        }
                        break;
                    case getDigitEmoji(6):
                        devin = true;
                        if(influence.length > 5) {
                            this.lance_actuel = influence[5];
                            await this.channel.send('Les dés affichent maintenant '+this.lance_actuel[0]+' et '+this.lance_actuel[1]+' avec les dés normaux et '+this.lance_actuel[2]+' avec le dé spécial');
                        } else {
                            await this.channel.send('Le lancé de dés n’a pas été modifié par le devin.');
                        }
                        break;
                    case getDigitEmoji(7):
                        devin = true;
                        if(influence.length > 6) {
                            this.lance_actuel = influence[6];
                            await this.channel.send('Les dés affichent maintenant '+this.lance_actuel[0]+' et '+this.lance_actuel[1]+' avec les dés normaux et '+this.lance_actuel[2]+' avec le dé spécial');
                        } else {
                            await this.channel.send('Le lancé de dés n’a pas été modifié par le devin.');
                        }
                        break;                             
                }
                if(devin || (collected.first().emoji.name == '🚪' && await this.removePlayer(this.roles.devin.joueur.id, collected.first().users.cache.last()))) {
                    break;
                }
            }

        }
    }

    async prisonnier() {
        if (this.roles.prisonnier.joueur != null && this.joueur_actuel !== this.roles.prisonnier.joueur && (this.lance_actuel.match(/3/g) || []).length > 0) {
            let sended = await this.channel.send('Le prisonnier <@'+this.roles.prisonnier.joueur.id+'> bois '+getDigitEmoji((this.lance_actuel.match(/3/g) || []).length)+' gorgée(s)');
            await sended.react('🍻');
            let filter = (reaction, user) => this.getPlayersId().includes(user.id)
            await sended.awaitReactions(filter, {max: 1});
            return false;
        }
        if (this.joueur_actuel === this.roles.prisonnier.joueur && (this.lance_actuel.match(/3/g) || []).length > 0) {
            let sended = await this.channel.send('Le prisonnier <@'+this.roles.prisonnier.joueur.id+'> sors de prison !!!\r\n Pour fêter ça, il bois '+getDigitEmoji(this.lance_actuel[2])+' gorgée(s)');
            await sended.react('🍻');
            let filter = (reaction, user) => this.getPlayersId().includes(user.id)
            await sended.awaitReactions(filter, {max: 1});
            if(this.lance_actuel.substr(0,2) == '32') {
                let sended = await this.channel.send('<@'+this.roles.prisonnier.joueur.id+'> retourne en prison !!!\r\n Pour fêter ça, il bois '+getDigitEmoji(this.lance_actuel[2])+' gorgée(s)');
                await sended.react('🍻');
                let filter = (reaction, user) => this.getPlayersId().includes(user.id)
                await sended.awaitReactions(filter, {max: 1});
            } else {
                await this.removeRole(this.roles.prisonnier.joueur, 'prisonnier');
            }
            return true;
        }
        if (this.joueur_actuel === this.roles.prisonnier.joueur && (this.lance_actuel.match(/3/g) || []).length == 0) {
            await this.channel.send('Le prisonnier <@'+this.roles.prisonnier.joueur.id+'> est toujours en prison ...');
            return true;
        }
        return false;
    }

    async attribuer_role() {
        let Player = this.joueur_actuel;
        let lance = this.lance_actuel;
        let action = false;
        for(let role in this.roles) {
            action = action || await this.roles[role].attribuer();
            if(action) { break; }
        }
        return action;
    }

    async jouer() {
        let d = new Date();
        console.log(d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+', '+d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds()+' --- Partie en cours :'+this.channel.guild.name+' - '+this.channel.name);
        if(await this.lancer_des(this.joueurs[this.aqui])) {
            await this.devin_influence();
            if (this.roles.clochard.joueur != null) { 
                let sended = await this.channel.send('<@'+this.roles.clochard.joueur.id+'>, espèce de clochard ! Tu bois '+getDigitEmoji(this.lance_actuel[2])+' gorgée(s)');
                await sended.react('🍻');
                let filter = (reaction, user) => this.getPlayersId().includes(user.id)
                await sended.awaitReactions(filter, {max: 1});
            }
            if (this.roles.demon.joueur != null) {
                await this.distribue_gorgees(this.roles.demon.joueur, this.lance_actuel[2], 'Le vil démon <@'+this.roles.demon.joueur.id+'> distribue '+getDigitEmoji(this.lance_actuel[2])+' gorgée(s)');
            }
            let action1 = await this.tout_le_monde_bois();
            let action2 = await this.duel_paysans(this.joueur_actuel);
            let action3 = await this.dieu_attaque(this.joueur_actuel);
            let finTour = await this.prisonnier();
			if (!finTour) {
				let action4 = false;
				if(action2 && this.duel_paysans_vainqueur) {
					var action5 = await this.attribuer_role();
				} else if(!action2) {
					var action5 = await this.attribuer_role();
				} else {
					var action5 = true;
				}
				if (this.lance_actuel[0] == '6') { action4 = true; await this.distribue_gorgees(this.joueur_actuel, this.lance_actuel[1], null); }
				if(!action1 && !action2 && !action3 && !action4 && !action5) {
					let sended = await this.channel.send('<@'+this.joueur_actuel.id+'> ton lancé ne sert a rien, tu bois '+getDigitEmoji(this.lance_actuel[2])+' gorgée(s) pour la peine!');
					await sended.react('🍻');
					let filter = (reaction, user) => this.getPlayersId().includes(user.id)
					await sended.awaitReactions(filter, {max: 1});
				}
			}
            return await this.suivant();
        } else {
            this.commence = null;
            return false;
        }
    }

    async reset(id) {
        let confirmed = true;
        let liste_joueurs = '';
        let liste_joueurs_id = new Array();
        this.joueurs.forEach(function (joueur) {
            if(joueur.id != id) {
                liste_joueurs = liste_joueurs + '<@'+joueur.id+'>, ';
                liste_joueurs_id.push(joueur.id);
            }
        })
        let sended = await this.channel.send('<@'+id+'> souhaite lancer une nouvelle partie sur ce channel.\r\n⚠️Cela supprimera la partie en cours⚠️\r\n'+liste_joueurs+'\r\n vous avez 30 secondes pour annuler la demande de <@'+id+'>');
        await sended.react('✅');
        await sended.react('❌');
        let filter = (reaction, user) => liste_joueurs_id.includes(user.id)
        let collected = await sended.awaitReactions(filter, {max: 1, time: 30000});
        if(collected.first() != null && collected.first().emoji.name == '❌') { 
            confirmed = false;
            await this.channel.send('La partie n’a pas été annulée.');
        }
        return confirmed;
    }

}

const Discord = require('discord.js'); //Ce que le bot à besoin /
const client = new Discord.Client(); //Que votre Bot est un nouvel utilisateur
var Jeu = new Object;

client.login("##TOKEN##"); //Token (Série de chiffre) propre a chaque Bot
client.on("ready", () => { //Signifie que le bot à bien démarré console.log("Je suis prêt !"); //Lorsque que le bot est lancé observer la console Visual Studio client.user.setGame("s'Update seul"); //Voir le Jeu sur le Discord
console.log('Liste des guilds :');
client.guilds.cache.forEach(function (guild) {
    console.log(guild.name);
});
console.log('-----------------');
client.user.setActivity(`Tapez "@`+client.user.username+`" dans un salon pour obtenir de l'aide`);

});
client.on('message', async function (message) {
    if (message.author.id !== client.user.id && message.mentions.users.has(client.user.id)) {
        if (Jeu[message.channel.id] == null) {
            Jeu[message.channel.id] = new Game(message.channel);
        }
        var commande = message.content.substr(4+client.user.id.length).trim();
        switch (commande) {
            case 'commencer':
                let confirmed = true;
                if(Jeu[message.channel.id].commence != null) {
                    confirmed = await Jeu[message.channel.id].reset(message.author.id);
                }
                if(confirmed) {
                    message.channel.send('Nouvelle partie initiée par <@'+message.author.id+'> :\r\nCliquez sur 🎲 pour participer. <@'+message.author.id+'>, quand tout le monde est prêt, clique sur ✅').then(sended => {
                        Jeu[message.channel.id] = new Game(message.channel);
                        Jeu[message.channel.id].commence = message.author.id;
                        Jeu[message.channel.id].newGameId = sended.id;
                        let d = new Date();
                        console.log(d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+', '+d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds()+' --- Partie démarrée :'+message.channel.guild.name+' - '+message.channel.name);
                        sended.react('🎲');
                        sended.react('✅');
                    }).catch(function (error) {
                        console.log(error);
                    });
                }
                break;
            case 'règles':
                Jeu[message.channel.id].definition();
                break;
            case 'résumé':
                Jeu[message.channel.id].resume();
                break;
            case 'stats':
                Jeu[message.channel.id].stats();
                break;
            case 'rejoindre':
                Jeu[message.channel.id].addPlayer(message.author.id, message.author.username);
                break;
            case 'quitter':
                Jeu[message.channel.id].removePlayer(message.author.id);
                break;
            case 'installer':
                let install = new Discord.MessageEmbed()
                    .setColor(0xffffff)
                    .setTitle('Lien d\'installation')
                    .setDescription('[Cliquez ici pour installer le bot sur votre serveur](https://discordapp.com/oauth2/authorize?client_id=695582556149776424&scope=bot&permissions=523328)')
                    .setFooter('Pour saisir une commande, mentionner le bot avant de saisir la commande');
                message.channel.send(install);
                break;
            default:
                let help = new Discord.MessageEmbed()
                    .setColor(0xffffff)
                    .setTitle('Liste des commandes')
                    .setDescription('règles : Affichez les règles générales du jeu.\r\ncommencer : Lance une nouvelle partie\r\nrejoindre : Rejoindre la partie en cours\r\nquitter : Quitter la partie\r\nrésumé : Affiche un résumé de la partie\r\nstats : Affiche les stats de la partie en cours\r\ninstaller : Affiche le lien pour installer ce bot sur votre serveur')
                    .setFooter('Pour saisir une commande, mentionner le bot avant de saisir la commande');
                message.channel.send(help);
                break;
        }
        /* Pour débug uniquement
        if (commande.substr(0,6) == 'piper ' && commande.substr(6).match(/\b\d{3}\b/g)) {
            console.log(commande.substr(6));
            Jeu[message.channel.id].des_pipes = commande.substr(6);
            message.delete();
        }
        //*/
    }
});
client.on('messageReactionRemove', async (reaction, user) => {
    if(reaction.message != null) {
        if (Jeu[reaction.message.channel.id] == null) {
            Jeu[reaction.message.channel.id] = new Game(reaction.message.channel);
        }
        if (reaction.partial) {
            // If the message this reaction belongs to was removed the fetching might result in an API error, which we need to handle
            try {
                await reaction.fetch();
            } catch (error) {
                console.log('Something went wrong when fetching the message: ', error);
                // Return as `reaction.message.author` may be undefined/null
                return;
            }
        }
        if (user.id !== client.user.id) {
            switch (reaction.message.id) {
                case Jeu[reaction.message.channel.id].newGameId:
                    if (reaction.emoji.name === '🎲') {
                        Jeu[reaction.message.channel.id].removePlayer(user.id);
                        if(reaction.count <= 1) {
                            reaction.message.reactions.removeAll();
                            reaction.message.react('🎲');
                            reaction.message.react('✅');
                        }
                    }
            }
        }
    }
});
client.on('messageReactionAdd', async (reaction, user) => {
    if(reaction.message != null) {
        if (Jeu[reaction.message.channel.id] == null) {
            Jeu[reaction.message.channel.id] = new Game(reaction.message.channel);
        }
        if (reaction.partial) {
            // If the message this reaction belongs to was removed the fetching might result in an API error, which we need to handle
            try {
                await reaction.fetch();
            } catch (error) {
                console.log('Something went wrong when fetching the message: ', error);
                // Return as `reaction.message.author` may be undefined/null
                return;
            }
        }
        if (user.id !== client.user.id) {
            switch (reaction.message.id) {
                case Jeu[reaction.message.channel.id].newGameId:
                    if (reaction.emoji.name === '🎲') {
                        Jeu[reaction.message.channel.id].addPlayer(user.id,user.username);
                        if(reaction.count == 2) {
                            reaction.users.remove(client.user.id);
                        }
                    }
                    if (reaction.emoji.name === '✅') {
                        if(user.id === Jeu[reaction.message.channel.id].commence) {
                            if(Jeu[reaction.message.channel.id].joueurs.length < 2) {
                                await reaction.message.channel.send('Il faut être au moins 2 joueurs.');
                                await reaction.users.remove(user.id);

                            } else {
                                Jeu[reaction.message.channel.id].jouer();
                                reaction.message.delete();
                            }
                        }
                    }
                    break;
                case Jeu[reaction.message.channel.id].reglesId:
                    Jeu[reaction.message.channel.id].showRoles();
            }
        }
    }
});
